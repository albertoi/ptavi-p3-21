#!/usr/bin/python3
# -*- coding: utf-8 -*-
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SMILHandler(ContentHandler):
    def __init__(self):

            self.lista = []
            self.width = ""
            self.height = ""
            self.background_color = ""
            self.id = ""
            self.top = ""
            self.bottom = ""
            self.left = ""
            self.right = ""
            self.src = ""
            self.begin = ""
            self.dur = ""
            self.region = ""


    def startElement(self, name, attrs):

        if name == "root-layout":
            root = {"rootlayout": ({"width": attrs.get("width", ""),
                                    "height": attrs.get("height", ""),
                                    "background-color": attrs.get("background-color")})}
            self.lista.append(root)


        elif name == "region":
            reg = {"region": ({"id": attrs.get("id", ""),
                                "top": attrs.get("top", ""),
                                "bottom": attrs.get("bottom", ""),
                                "left": attrs.get("left", ""),
                                "right": attrs.get("right", "")})}
            self.lista.append(reg)

        elif name == "img":
            img = {"img": ({"src": attrs.get("src", ""),
                            "region": attrs.get("region", ""),
                            "begin": attrs.get("begin", ""),
                            "dur": attrs.get("dur", "")})}
            self.lista.append(img)

        elif name == "audio":
            audio = {"audio": ({"src": attrs.get("src", ""),
                                "begin": attrs.get("begin", ""),
                                "dur": attrs.get("dur", "")})}
            self.lista.append(audio)

        elif name == "textstream":
            text = {"textstream": ({"src": attrs.get("src", ""),
                                    "region": attrs.get("region", "")})}
            self.lista.append(text)

    def get_tags(self):
        """Método que devuelve la lista"""
        return self.lista



def main():
    """Programa principal"""
    parser = make_parser()
    cHandler = SMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    print(cHandler.get_tags())

if __name__ == "__main__":
    main()

