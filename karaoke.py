#!/usr/bin/python3
# -*- coding: utf-8 -*-


from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import smil
import sys
objeto = smil.SMILHandler
lista = objeto.get_tags()

def to_string(lista):
    resultado = ""
    for formato in lista:
        name = formato[0]
        atrib = formato[1]
        resultado = str(name)
        for atrix in atrib:
            if atrib[atrix]:
                resultado = (str('\t' + atrix + '="' + atrib[atrix] + '"'))
        resultado += "\n"
    return resultado

def main():
    """Programa principal"""
    parser = make_parser()
    cHandler = SMILHandler()
    print(lista)
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    print(cHandler.get_tags())

if __name__ == "__main__":
    main()